/**
 * Created by eric on 12/26/16.
 */
public class HeapSort {

  private static int[] a;
  private static int n;

  public static void main(String[] args) {
    int[] array = new int[]{1, 3, 2, 7, 6, 4, 5};

    System.out.println("Unsorted: ");
    for (int i : array) {
      System.out.print(i + " ");
    }
    System.out.println();

    sort(array);

    System.out.println("Sorted: ");
    for (int i : array) {
      System.out.print(i + " ");
    }
    System.out.println();
  }

  private static void buildHeap(int[] a) {
    n = a.length - 1;
    for (int i = n / 2; i >= 0; i--) {
      maxHeap(a, i);
    }
  }

  private static void maxHeap(int[] a, int i) {
    int left = 2 * i;
    int right = 2 * i + 1;
    int largest;
    if (left <= n && a[left] > a[i]) {
      largest = left;
    } else {
      largest = i;
    }

    if (right <= n && a[right] > a[largest]) {
      largest = right;
    }
    if (largest != i) {
      exchange(i, largest);
      maxHeap(a, largest);
    }
  }

  private static void exchange(int i, int j) {
    int t = a[i];
    a[i] = a[j];
    a[j] = t;
  }


  private static void sort(int[] a0) {
    a = a0;
    buildHeap(a);

    for (int i = n; i > 0; i--) {
      exchange(0, i);
      n = n - 1;
      maxHeap(a, 0);
    }
  }

}
