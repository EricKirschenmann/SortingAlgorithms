/**
 * Created by eric on 12/26/16.
 */
public class SelectionSort {

  public static void main(String[] args) {
    int[] array = new int[]{1, 3, 2, 7, 6, 4, 5};
    System.out.println("Unsorted: ");
    for (int i : array) {
      System.out.print(i + " ");
    }
    System.out.println();

    selectionSort(array);

    System.out.println("Sorted: ");
    for (int i : array) {
      System.out.print(i + " ");
    }
  }

  /**
   * Sorts an array of integers using Selection sort
   *
   * @param array Array of integers to be sorted
   * @return Array of sorted integers
   */
  private static int[] selectionSort(int[] array) {
    for (int i = 0; i < array.length - 1; i++) {
      int index = i;
      for (int j = i + 1; j < array.length; j++) {
        if (array[j] < array[index]) {
          index = j;
        }
      }

      int smallerNumber = array[index];
      array[index] = array[i];
      array[i] = smallerNumber;
    }
    return array;
  }
}
