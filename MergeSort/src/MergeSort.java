/**
 * Created by eric on 12/26/16.
 */
public class MergeSort {
  private static int[] array;
  private static int[] tempArray;

  public static void main(String[] args) {
    int[] arr = new int[]{1, 3, 2, 7, 6, 4, 5};

    System.out.println("Unsorted: ");
    for (int i : arr) {
      System.out.print(i + " ");
    }

    System.out.println();
    sort(arr);

    System.out.println("Sorted: ");
    for (int i : arr) {
      System.out.print(i + " ");
    }
  }

  private static void sort(int inputArr[]) {
    array = inputArr;
    int length = inputArr.length;
    tempArray = new int[length];
    mergeSort(0, length - 1);
  }

  private static void mergeSort(int lowerIndex, int higherIndex) {

    if (lowerIndex < higherIndex) {
      int middle = lowerIndex + (higherIndex - lowerIndex) / 2;
      // Below step sorts the left side of the array
      mergeSort(lowerIndex, middle);
      // Below step sorts the right side of the array
      mergeSort(middle + 1, higherIndex);
      // Now merge both sides
      mergeParts(lowerIndex, middle, higherIndex);
    }
  }

  private static void mergeParts(int lowerIndex, int middle, int higherIndex) {
    System.arraycopy(array, 0, tempArray, 0, array.length);

    int i = lowerIndex;
    int j = middle + 1;
    int k = lowerIndex;
    while (i <= middle && j <= higherIndex) {
      if (tempArray[i] <= tempArray[j]) {
        array[k] = tempArray[i];
        i++;
      } else {
        array[k] = tempArray[j];
        j++;
      }
      k++;
    }
    while (i <= middle) {
      array[k] = tempArray[i];
      k++;
      i++;
    }

  }
}
