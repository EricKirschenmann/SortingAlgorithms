/**
 * Created by eric on 12/26/16.
 */
public class InsertionSort {

  public static void main(String[] args) {
    int[] array = new int[]{1, 3, 2, 7, 6, 4, 5};

    System.out.println("Unsorted: ");
    for (int i : array) {
      System.out.print(i + " ");
    }
    System.out.println();

    // sort array
    insertionSort(array);

    System.out.println("Sorted: ");
    for (int i : array) {
      System.out.print(i + " ");
    }
    System.out.println();
  }

  /**
   * Sorts an array of integers using insertion sort
   *
   * @param array Array of integers to be sorted
   * @return Array of sorted integers
   */
  private static int[] insertionSort(int[] array) {
    int temp;
    for (int i = 1; i < array.length; i++) {
      for (int j = i; j > 0; j--) {
        if (array[j] < array[j - 1]) {
          temp = array[j];
          array[j] = array[j - 1];
          array[j - 1] = temp;
        }
      }
    }
    return array;
  }
}