import java.util.Random;

/**
 * Created by eric on 2/9/17.
 */
public class BogoSort {

  public static void main(String[] args) {
    // String to be sorted
    String sorted = "bubby is the greatest";
    String unsorted = randomize(sorted); // start the randomization

    // count iterations
    int count = 1;

    // randomly sort the "unsorted" String and check if it is sorted
    while (!sorted.equals(unsorted)) {
      unsorted = randomize(unsorted); // re-randomize the string
      System.out.println(count + ": " + unsorted); // display the newly randomized string

      if (count == Integer.MAX_VALUE - 1) {
        count = 0;
      } else {
        count++;
      }
    }

  }

  /**
   * Takes in a {@code String} and returns a randomized version
   *
   * @param s The {@code String} to randomize
   * @return The newly randomized {@code String}
   */
  private static String randomize(String s) {

    // StringBuilder lets you set the individual characters
    StringBuilder sb = new StringBuilder(s);

    // randomize the letters
    Random rand = new Random();
    for (int i = sb.length() - 1; i > 1; i--) {
      int swapWith = rand.nextInt(i);
      char tmp = sb.charAt(swapWith);
      sb.setCharAt(swapWith, sb.charAt(i));
      sb.setCharAt(i, tmp);
    }

    return sb.toString();
  }

}
